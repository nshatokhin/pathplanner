#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_actionCreate_triggered();

    void on_actionStart_search_triggered();

    void on_removeNodeBtn_toggled(bool checked);

    void on_addNodeBtn_toggled(bool checked);

    void on_setSourceBtn_toggled(bool checked);

    void on_setTargetBtn_toggled(bool checked);

    void on_actionSave_triggered();

    void on_actionLoad_triggered();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
