#include "utils.h"

bool isEqual(double a, double b)
{
    return std::fabs(a - b) < std::numeric_limits<double>::epsilon();
}
