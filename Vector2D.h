#pragma once

#include "Coordinate2D.h"

class Vector2D : public Coordinate2D
{
public:
	Vector2D(coordinate_t x = 0, coordinate_t y = 0) : Coordinate2D(x, y) {}
	Vector2D(const Coordinate2D &start, const Coordinate2D &end) : Vector2D(end.x - start.x, end.y - start.y) {}

	double length() const;
	double lengthSqr() const;
	Vector2D unitVector() const;

	Vector2D crossProduct(const Vector2D &other);
	double dotProduct(const Vector2D &other) const;

	Vector2D perpendicular();

	Vector2D rotate(const Coordinate2D &centerOfRotation, double angle) const;

	friend Vector2D operator*(const Vector2D &left, double right);
	friend Vector2D operator*(double left, const Vector2D &right);

	friend Vector2D operator/(const Vector2D &left, double right);

	friend Vector2D operator+(const Vector2D &left, const Vector2D &right);
	friend Vector2D operator-(const Vector2D &left, const Vector2D &right);
};