#-------------------------------------------------
#
# Project created by QtCreator 2019-10-25T10:06:46
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = PathFinder
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
        BFSSearch.cpp \
        DFSSearch.cpp \
        DijkstraSearch.cpp \
        Graph.cpp \
        Vector2D.cpp \
        field.cpp \
        main.cpp \
        mainwindow.cpp \
        pathfinder.cpp \
        utils.cpp

HEADERS += \
        AStarSearch.h \
        BFSSearch.h \
        Common.h \
        Coordinate2D.h \
        DFSSearch.h \
        DStarLiteKey.h \
        DStarLiteSearch.h \
        DijkstraSearch.h \
        Edge.h \
        Graph.h \
        Helpers.h \
        Heuristic.h \
        Node.h \
        Vector2D.h \
        field.h \
        mainwindow.h \
        pathfinder.h \
        pq/MinHeap.h \
        pq/MinHeapIndexed.h \
        pq/PriorityQueue.h \
        utils.h

FORMS += \
        mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
