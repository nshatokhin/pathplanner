#include "Graph.h"

#include <algorithm>
#include <cassert>

Graph::Graph(IdxType maxNodes, bool isDigraph) :
	m_digraph(isDigraph),
	m_maxNodesCount(maxNodes),
	m_nodesCount(0),
    m_nodes(maxNodes), m_edges(maxNodes), m_predecessorEdges(maxNodes)
{
}

Graph::~Graph()
{
}

const Graph::NodeType & Graph::GetNode(IdxType idx) const
{
	assert((idx < m_nodesCount) && (idx >= 0));

	return m_nodes[idx];
}

Graph::NodeType & Graph::GetNode(IdxType idx)
{
	assert((idx < m_nodesCount) && (idx >= 0));

	return m_nodes[idx];
}

const Graph::EdgeType & Graph::GetEdge(IdxType from, IdxType to) const
{
	assert((from < m_nodesCount) && (from >= 0) && m_nodes[from].index != invalid_node_index);

	assert((to < m_nodesCount) && (to >= 0) && m_nodes[to].index != invalid_node_index);

	for (EdgeList::const_iterator curEdge = m_edges[from].begin();
		curEdge != m_edges[from].end();
		++curEdge)
	{
		if (curEdge->to == to) return *curEdge;
	}

	assert(false);
}

Graph::EdgeType & Graph::GetEdge(IdxType from, IdxType to)
{
	assert((from < m_nodesCount) && (from >= 0) && m_nodes[from].index != invalid_node_index);

	assert((to < m_nodes.size()) && (to >= 0) && m_nodes[to].index != invalid_node_index);

	for (EdgeList::iterator curEdge = m_edges[from].begin();
		curEdge != m_edges[from].end();
		++curEdge)
	{
		if (curEdge->to == to) return *curEdge;
	}

    assert(false);
}

const Graph::EdgeType & Graph::GetPredecessorEdge(IdxType from, IdxType to) const
{
    assert((from < m_nodesCount) && (from >= 0) && m_nodes[from].index != invalid_node_index);

    assert((to < m_nodesCount) && (to >= 0) && m_nodes[to].index != invalid_node_index);

    for (EdgeList::const_iterator curEdge = m_predecessorEdges[to].begin();
        curEdge != m_predecessorEdges[to].end();
        ++curEdge)
    {
        if (curEdge->from == from) return *curEdge;
    }

    assert(false);
}

Graph::EdgeType & Graph::GetPredecessorEdge(IdxType from, IdxType to)
{
    assert((from < m_nodesCount) && (from >= 0) && m_nodes[from].index != invalid_node_index);

    assert((to < m_nodes.size()) && (to >= 0) && m_nodes[to].index != invalid_node_index);

    for (EdgeList::iterator curEdge = m_predecessorEdges[to].begin();
        curEdge != m_predecessorEdges[to].end();
        ++curEdge)
    {
        if (curEdge->from == from) return *curEdge;
    }

    assert(false);
}


const Graph::EdgeList &Graph::GetEdges(IdxType node) const
{
    assert((node < m_nodesCount) && (node >= 0));

    return m_edges[node];
}

Graph::EdgeList &Graph::GetEdges(IdxType node)
{
    assert((node < m_nodesCount) && (node >= 0));

    return m_edges[node];
}

const Graph::EdgeList &Graph::GetPredecessorEdges(IdxType node) const
{
    assert((node < m_nodesCount) && (node >= 0));

    return m_predecessorEdges[node];
}

Graph::EdgeList &Graph::GetPredecessorEdges(IdxType node)
{
    assert((node < m_nodesCount) && (node >= 0));

    return m_predecessorEdges[node];
}

IdxType Graph::addNode(const Node &node)
{
	assert(m_nodesCount < m_maxNodesCount);

	if (node.index < m_nodesCount)
	{
		//make sure the client is not trying to add a node with the same ID as
		//a currently active node
		assert(m_nodes[static_cast<NodeIndex>(node.index)].index == invalid_node_index);

		m_nodes[static_cast<NodeIndex>(node.index)] = node;

		return m_nodesCount;
	}
	else
	{
		//make sure the new node has been indexed correctly
		assert(node.index == m_nodesCount);

		m_nodes[m_nodesCount] = node;
		m_edges[m_nodesCount] = EdgeList();

		return m_nodesCount++;
	}
}

void Graph::addEdge(const Edge &edge)
{
	//first make sure the from and to nodes exist within the graph 
	assert((edge.from < m_nodesCount) && (edge.to < m_nodesCount));

	//make sure both nodes are active before adding the edge
	if ((m_nodes[edge.to].index != invalid_node_index) &&
		(m_nodes[edge.from].index != invalid_node_index))
	{
		//add the edge, first making sure it is unique
		if (isUniqueEdge(edge.from, edge.to))
		{
			m_edges[edge.from].push_back(edge);
            m_predecessorEdges[edge.to].push_back(edge);
		}

		//if the graph is undirected we must add another connection in the opposite
		//direction
		if (!m_digraph)
		{
			//check to make sure the edge is unique before adding
			if (isUniqueEdge(edge.to, edge.from))
			{
				EdgeType NewEdge = edge;

				NewEdge.to = edge.from;
				NewEdge.from = edge.to;

				m_edges[edge.to].push_back(NewEdge);
                m_predecessorEdges[edge.from].push_back(NewEdge);
			}
		}
	}
}

void Graph::removeNode(IdxType node)
{
	assert(node < m_nodesCount);

	//set this node's index to invalid_node_index
	m_nodes[node].index = invalid_node_index;

	//if the graph is not directed remove all edges leading to this node and then
	//clear the edges leading from the node
	if (!m_digraph)
	{
		//visit each neighbour and erase any edges leading to this node
		for (EdgeList::iterator curEdge = m_edges[node].begin();
			curEdge != m_edges[node].end();
			++curEdge)
		{
			for (EdgeList::iterator curE = m_edges[curEdge->to].begin();
				curE != m_edges[curEdge->to].end();
				++curE)
			{
				if (curE->to == node)
				{
					curE = m_edges[curEdge->to].erase(curE);

					break;
				}
			}
		}

        for (EdgeList::iterator curEdge = m_predecessorEdges[node].begin();
            curEdge != m_predecessorEdges[node].end();
            ++curEdge)
        {
            for (EdgeList::iterator curE = m_predecessorEdges[curEdge->from].begin();
                curE != m_predecessorEdges[curEdge->from].end();
                ++curE)
            {
                if (curE->from == node)
                {
                    curE = m_predecessorEdges[curEdge->from].erase(curE);

                    break;
                }
            }
        }

		//finally, clear this node's edges
		m_edges[node].clear();
        m_predecessorEdges[node].clear();
	}

	//if a digraph remove the edges the slow way
	else
	{
		removeInvalidEdges();
	}
}

void Graph::removeEdge(IdxType from, IdxType to)
{
	assert((from < m_nodesCount) && (to < m_nodesCount));

	EdgeList::iterator curEdge;

	if (!m_digraph)
	{
		for (curEdge = m_edges[to].begin();
			curEdge != m_edges[to].end();
			++curEdge)
		{
			if (curEdge->to == from) { curEdge = m_edges[to].erase(curEdge); break; }
		}

        for (curEdge = m_predecessorEdges[from].begin();
            curEdge != m_predecessorEdges[from].end();
            ++curEdge)
        {
            if (curEdge->from == to) { curEdge = m_predecessorEdges[from].erase(curEdge); break; }
        }
	}

	for (curEdge = m_edges[from].begin();
		curEdge != m_edges[from].end();
		++curEdge)
	{
		if (curEdge->to == to) { curEdge = m_edges[from].erase(curEdge); break; }
	}

    for (curEdge = m_predecessorEdges[to].begin();
        curEdge != m_predecessorEdges[to].end();
        ++curEdge)
    {
        if (curEdge->from == from) { curEdge = m_predecessorEdges[to].erase(curEdge); break; }
    }
}

void Graph::setEdgeCost(IdxType from, IdxType to, double cost)
{
	//make sure the nodes given are valid
	assert((from < m_nodes.size()) && (to < m_nodes.size()));

	for (EdgeList::iterator curEdge = m_edges[from].begin();
		curEdge != m_edges[from].end();
		++curEdge)
	{
		if (curEdge->to == to)
		{
			curEdge->cost = cost;
			break;
		}
	}

    for (EdgeList::iterator curEdge = m_predecessorEdges[to].begin();
        curEdge != m_predecessorEdges[to].end();
        ++curEdge)
    {
        if (curEdge->from == from)
        {
            curEdge->cost = cost;
            break;
        }
    }
}

bool Graph::isNodePresent(IdxType nd) const
{
	if ((m_nodes[nd].index == invalid_node_index) || (nd >= m_nodesCount))
	{
		return false;
	}
	else
	{
		return true;
	}
}

bool Graph::isEdgePresent(IdxType from, IdxType to) const
{
    if (isNodePresent(from) && isNodePresent(to))
	{
		for (EdgeList::const_iterator curEdge = m_edges[from].begin();
			curEdge != m_edges[from].end();
			++curEdge)
		{
			if (curEdge->to == to) return true;
		}

		return false;
	}
	else
	{
		return false;
	}
}

bool Graph::isPredecessorEdgePresent(IdxType from, IdxType to) const
{
    if (isNodePresent(from) && isNodePresent(to))
    {
        for (EdgeList::const_iterator curEdge = m_predecessorEdges[to].begin();
            curEdge != m_predecessorEdges[to].end();
            ++curEdge)
        {
            if (curEdge->from == from) return true;
        }

        return false;
    }
    else
    {
        return false;
    }
}

bool Graph::isUniqueEdge(IdxType from, IdxType to)
{
	for (EdgeList::const_iterator curEdge = m_edges[from].begin();
		curEdge != m_edges[from].end();
		++curEdge)
	{
		if (curEdge->to == to)
		{
			return false;
		}
	}

	return true;
}

bool Graph::isUniquePredecessorEdge(IdxType from, IdxType to)
{
    for (EdgeList::const_iterator curEdge = m_predecessorEdges[to].begin();
        curEdge != m_predecessorEdges[to].end();
        ++curEdge)
    {
        if (curEdge->from == from)
        {
            return false;
        }
    }

    return true;
}

void Graph::removeInvalidEdges()
{
	for (EdgeListVector::iterator curEdgeList = m_edges.begin(); curEdgeList != m_edges.end(); ++curEdgeList)
	{
		for (EdgeList::iterator curEdge = (*curEdgeList).begin(); curEdge != (*curEdgeList).end(); ++curEdge)
		{
			if (m_nodes[curEdge->to].index == invalid_node_index ||
				m_nodes[curEdge->from].index == invalid_node_index)
			{
				curEdge = (*curEdgeList).erase(curEdge);
			}
		}
	}

    for (EdgeListVector::iterator curEdgeList = m_predecessorEdges.begin(); curEdgeList != m_predecessorEdges.end(); ++curEdgeList)
    {
        for (EdgeList::iterator curEdge = (*curEdgeList).begin(); curEdge != (*curEdgeList).end(); ++curEdge)
        {
            if (m_nodes[curEdge->to].index == invalid_node_index ||
                m_nodes[curEdge->from].index == invalid_node_index)
            {
                curEdge = (*curEdgeList).erase(curEdge);
            }
        }
    }
}

bool Graph::save(const char* fileName) const
{
    //open the file and make sure it's valid
    std::ofstream out(fileName);

    if (!out)
    {
        throw std::runtime_error("Cannot open file: " + std::string(fileName));
        return false;
    }

    return save(out);
}

bool Graph::save(std::ofstream& stream) const
{
    //save the number of nodes
    stream << m_nodesCount << std::endl;

    //iterate through the graph nodes and save them
    NodeVector::const_iterator curNode = m_nodes.begin();
    for (; curNode!=m_nodes.end(); ++curNode)
    {
        stream << *curNode;
    }

    //save the number of edges
    stream << numEdges() << std::endl;


    //iterate through the edges and save them
    for (IdxType nodeIdx = 0; nodeIdx < m_nodesCount; ++nodeIdx)
    {
        for (EdgeList::const_iterator curEdge = m_edges[nodeIdx].begin();
             curEdge!=m_edges[nodeIdx].end(); ++curEdge)
        {
            stream << *curEdge;
        }
    }

    return true;
}

bool Graph::load(const char* fileName)
{
    //open file and make sure it's valid
    std::ifstream in(fileName);

    if (!in)
    {
        throw std::runtime_error("Cannot open file: " + std::string(fileName));
        return false;
    }

    return load(in);
}

bool Graph::load(std::ifstream& stream)
{
    Clear();

    //get the number of nodes and read them in
    IdxType numNodes, numEdges;

    stream >> numNodes;

    for (IdxType n=0; n<numNodes; ++n)
    {
        NodeType newNode(stream);

        addNode(newNode);
    }

    //now add the edges
    stream >> numEdges;
    for (IdxType e=0; e<numEdges; ++e)
    {
        EdgeType nextEdge(stream);

        m_edges[nextEdge.from].push_back(nextEdge);
        m_predecessorEdges[nextEdge.to].push_back(nextEdge);
    }

    return true;
}
