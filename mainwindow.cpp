#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QFileDialog>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);


}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_actionCreate_triggered()
{
    ui->widget->init(30, 30);
}

void MainWindow::on_actionStart_search_triggered()
{
    ui->widget->search();
}

void MainWindow::on_removeNodeBtn_toggled(bool checked)
{
    if(checked)
    {
        ui->widget->setMode(Field::Mode::DeleteNode);
    }
}

void MainWindow::on_addNodeBtn_toggled(bool checked)
{
    if(checked)
    {
        ui->widget->setMode(Field::Mode::AddNode);
    }
}

void MainWindow::on_setSourceBtn_toggled(bool checked)
{
    if(checked)
    {
        ui->widget->setMode(Field::Mode::SetSource);
    }
}

void MainWindow::on_setTargetBtn_toggled(bool checked)
{
    if(checked)
    {
        ui->widget->setMode(Field::Mode::SetTarget);
    }
}

void MainWindow::on_actionSave_triggered()
{
    QString fileName = QFileDialog::getSaveFileName(this,
            tr("Save Graph"), "",
            tr("Graph (*.grp);;All Files (*)"));

    if(!fileName.isEmpty())
    {
        ui->widget->save(fileName.toStdString().c_str());
    }
}

void MainWindow::on_actionLoad_triggered()
{
    QString fileName = QFileDialog::getOpenFileName(this,
            tr("Load graph"), "",
            tr("Graph (*.grp);;All Files (*)"));

    if(!fileName.isEmpty())
    {
        ui->widget->load(fileName.toStdString().c_str());
    }
}
