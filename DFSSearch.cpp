#include "DFSSearch.h"

bool DFSSearch::Search(IdxType source, IdxType target)
{
#ifdef DEBUG
    m_spanningTree.clear();
#endif

    Graph::EdgeType dummyEdge(source, source, 0);

    m_pathExists = false;

    m_source = source;
    m_target = target;

    m_stack.push(&dummyEdge);

    while (m_stack.size() > 0)
    {
        const Graph::EdgeType * edge = m_stack.top();

        m_stack.pop();

        m_route[edge->to] = edge->from;

#ifdef DEBUG
        //put it on the tree. (making sure the dummy edge is not placed on the tree)
        if (edge != &dummyEdge)
        {
          m_spanningTree.push_back(edge);
        }
#endif

        m_visited[edge->from] = true;

        if(edge->to == target)
        {
            m_pathExists = true;
            return true;
        }

        Graph::ConstEdgeIterator edges(m_graph, edge->to);
        for (const Graph::EdgeType * curEdge = edges.begin();
                    !edges.end();
                    curEdge = edges.next())
        {
            if(!m_visited[curEdge->to])
            {
                m_stack.push(curEdge);
            }
        }
    }

    return false;
}

std::list<IdxType> DFSSearch::GetPath()
{
    std::list<IdxType> path;

    if(!m_pathExists || m_target < 0)
    {
        return  path;
    }

    IdxType node = static_cast<IdxType>(m_target);

    path.push_back(node);

    while(node != m_source)
    {
        node = static_cast<IdxType>(m_route[node]);

        path.push_back(node);
    }

    return path;
}
