#pragma once

#include <cstdint>

typedef uint32_t IdxType;
typedef int32_t CoordType;
typedef int64_t NodeIndex;

#define invalid_node_index -1

#define DEBUG
