#pragma once

#include <cassert>
#include <cstdint>
#include <cstring>
#include <memory>

template<typename ObjectType, typename IdxType = std::size_t>
class MinHeapIndexed
{
public:
        MinHeapIndexed(ObjectType * objects, IdxType maxElements, ObjectType minusInfiniy, ObjectType plusInfinity) :
		m_heapSize(0), m_maxSize(maxElements), m_minusInfinity(minusInfiniy), m_plusInfinity(plusInfinity)
	{
		assert(maxElements > 0);

        m_objects = objects;
        m_objectIndices = new ObjectType[m_maxSize];
		m_externalIndices = new IdxType[maxElements];
		m_internalIndices = new IdxType[maxElements];

		for (IdxType i = 0; i < maxElements; i++)
		{
            m_objectIndices[i] = i;
			m_externalIndices[i] = i;
			m_internalIndices[i] = i;
		}
	}

        ~MinHeapIndexed()
	{
        delete[] m_objectIndices;
		delete[] m_externalIndices;
		delete[] m_internalIndices;
	}
	
        MinHeapIndexed(const MinHeapIndexed &other)
	{
		if(other.m_heapSize <= m_maxSize)
		{
			m_heapSize = other.m_heapSize;
		}
		else
		{
			m_heapSize = m_maxSize;
		}
		
        m_objects = other.m_objects;
        std::memcpy(m_objectIndices, other.m_objectIndices, sizeof(IdxType) * m_heapSize);
		std::memcpy(m_externalIndices, other.m_externalIndices, sizeof(IdxType) * m_heapSize);
		std::memcpy(m_internalIndices, other.m_internalIndices, sizeof(IdxType) * m_heapSize);
	}

	IdxType heapSize() const
	{
		return m_heapSize;
	}

	const ObjectType * objects() const
	{
		return m_objects;
	}

	const IdxType * indices() const
	{
		return m_externalIndices;
	}

	IdxType * buildHeap(ObjectType * array, IdxType elementsCount)
	{
		assert(elementsCount <= m_maxSize);

        m_objects = array;

        for(IdxType i = 0; i <= elementsCount; i++)
        {
            m_objectIndices[i] = i;
        }

		std::memcpy(m_objects, array, sizeof(ObjectType)*elementsCount);

		m_heapSize = elementsCount;
		for (IdxType i = 0; i <= m_heapSize / 2; i++)
		{
			siftDown(m_heapSize / 2 - i);
		}

		return m_externalIndices;
	}

	ObjectType min()
	{
		if (m_heapSize == 0)
			return m_plusInfinity;

        return m_objects[m_objectIndices[0]];
	}

	ObjectType extractMin()
	{
		if (m_heapSize == 0)
			return m_plusInfinity;

        ObjectType min = m_objects[m_objectIndices[0]];

		if (m_heapSize - 1 != 0)
		{
			exchangeObjects(0, m_heapSize - 1);
		}

		m_heapSize--;
		siftDown(0);

		return min;
	}

    IdxType insert(IdxType idx)
	{
		assert(m_heapSize < m_maxSize);

		m_heapSize++;
		IdxType index = m_externalIndices[m_heapSize - 1];
        m_objectIndices[m_heapSize - 1] = idx;
		siftUp(m_heapSize - 1);

		return index;
	}

    void update(IdxType i, const ObjectType &obj)
	{
		assert(i < m_maxSize && m_internalIndices[i] < m_heapSize);

        ObjectType &old = m_objects[m_objectIndices[m_internalIndices[i]]];

        if (old < obj)
		{
			old = obj;
			siftDown(0);
		}
        else if (old > obj)
		{
			old = obj;
			siftUp(m_internalIndices[i]);
		}
	}

	void remove(IdxType i)
	{
		update(i, m_minusInfinity);
		extractMin();
	}

protected:
	void exchangeObjects(IdxType obj1, IdxType obj2)
	{
        ObjectType tempObj = m_objectIndices[obj1];
        m_objectIndices[obj1] = m_objectIndices[obj2];
        m_objectIndices[obj2] = tempObj;
		
		IdxType tempIdx = m_internalIndices[m_externalIndices[obj1]];
		m_internalIndices[m_externalIndices[obj1]] = m_internalIndices[m_externalIndices[obj2]];
		m_internalIndices[m_externalIndices[obj2]] = tempIdx;

		tempIdx = m_externalIndices[obj1];
		m_externalIndices[obj1] = m_externalIndices[obj2];
		m_externalIndices[obj2] = tempIdx;
	}

	void siftDown(IdxType i)
	{
		IdxType left, right, j;
		while (2 * i + 1 < m_heapSize)
		{
			left = 2 * i + 1;
			right = 2 * i + 2;

			j = left;

            if (right < m_heapSize && m_objects[m_objectIndices[right]] < m_objects[m_objectIndices[left]])
			{
				j = right;
			}

            if (m_objects[m_objectIndices[i]] <= m_objects[m_objectIndices[j]])
			{
				break;
			}

			exchangeObjects(i, j);
			i = j;
		}
	}

	void siftUp(IdxType i)
	{
		IdxType parent = (i - 1) / 2;
        while (i > 0 && m_objects[m_objectIndices[i]] < m_objects[m_objectIndices[parent]])
		{
			exchangeObjects(i, parent);
			i = parent;
			parent = (i - 1) / 2;
		}
	}

protected:
	IdxType m_heapSize, m_maxSize;

    ObjectType * m_objects;
    IdxType * m_objectIndices;
	IdxType * m_externalIndices, * m_internalIndices;
	ObjectType m_minusInfinity, m_plusInfinity;
};
