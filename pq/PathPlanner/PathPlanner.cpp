#include "PathPlanner.h"

PathPlanner::PathPlanner(size_t reservedMemory) :
	m_pq(
		reservedMemory,
		PQKey(std::numeric_limits<double>::max(), std::numeric_limits<double>::max()),
		PQValue(std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), -1.0)
	)
{

}
