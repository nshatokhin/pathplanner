#pragma once

#include "structures/PriorityQueue.h"

class PQKey
{
public:
	PQKey(double l, double r) : left(l), right(r) {};

	bool operator==(const PQKey &other) const
	{
		return left == other.left && right == other.right;
	}

	bool operator!=(const PQKey &other) const
	{
		return left != other.left || right != other.right;
	}

	bool operator > (const PQKey &other) const {
		if (left - EPS > other.left) return true;
		else if (left < other.left - EPS) return false;
		return right > other.right;
	}

	bool operator <= (const PQKey &other) const {
		if (left < other.left) return true;
		else if (left > other.left) return false;
		return right < other.right + EPS;
	}


	bool operator < (const PQKey &other) const {
		if (left + EPS < other.left) return true;
		else if (left - EPS > other.left) return false;
		return right < other.right;
	}

public:
	double left, right;

protected:
	static constexpr double EPS = DBL_EPSILON;//0.00001;
};

class PQValue
{
	PQValue(uint32_t xv = 0, uint32_t yv = 0, double phsv = std::numeric_limits<double>::max(),
		double gv = std::numeric_limits<double>::max(), double costv = -1.0) :
		x(xv), y(yv),
		phs(phsv), g(gv),
		cost(costv)
	{}

	bool operator==(const PQValue &other) const
	{
		return x == other.x && y == other.y;
	}

	bool operator!=(const PQValue &other) const
	{
		return x != other.x || y != other.y;
	}
public:
	uint32_t x, y;
	double phs, g, cost;
};

class PathPlanner
{
public:
	PathPlanner(size_t reservedMemory);

protected:
	PriorityQueue<PQKey, PQValue> m_pq;
};