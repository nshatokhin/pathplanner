#include "field.h"

#include <QDebug>
#include <QMouseEvent>
#include <QPainter>
#include <QPen>

#include "Helpers.h"

Field::Field(QWidget *parent) : QWidget(parent),
    m_cellsWidthCount(30),
    m_cellsHeightCount(30),
    m_currentMode(Mode::DeleteNode),
    m_sourceNode(-1),
    m_destNode(-1),
    graph(m_cellsWidthCount*m_cellsHeightCount),
    dfs(graph),
    bfs(graph),
    deikstra(graph),
    aStar(graph, m_busyVector),
    dStar(graph)
{

}

void Field::init(uint32_t cellsXCount, uint32_t cellsYCount)
{
    m_cellsWidthCount = cellsXCount;
    m_cellsHeightCount = cellsYCount;

    m_busyVector = std::vector<bool>(m_cellsWidthCount*m_cellsHeightCount, false);

    m_cellWidth = static_cast<uint32_t>(width()) / m_cellsWidthCount;
    m_cellHeight = static_cast<uint32_t>(height()) / m_cellsHeightCount;

    graph.Clear();

    Graph_CreateGrid(graph, m_cellWidth, m_cellHeight, m_cellsWidthCount, m_cellsHeightCount);

    m_sourceNode = 23;
    m_destNode = 78;

    m_busyVector[53] = true;
    m_busyVector[68] = true;

    aStar.Init(m_sourceNode, m_destNode);

    repaint();
}

void Field::search()
{
    aStar.Init(m_sourceNode, m_destNode);



    qDebug() << "Search:" << aStar.Search();

    repaint();
}

void Field::mousePressEvent(QMouseEvent *event)
{
    qDebug() << "Click: " << event->x() << event->y();

    uint32_t cellX = event->x() / m_cellWidth;
    uint32_t cellY = event->y() / m_cellHeight;

    qDebug() << "Cell id: " << cellX << cellY;

    if(m_currentMode == Mode::DeleteNode)
    {
        deleteNode(cellX, cellY);
    }
    else if(m_currentMode == Mode::AddNode)
    {
        addNode(cellX, cellY);
    }
    else if(m_currentMode == Mode::SetSource)
    {
        setSource(cellX, cellY);
    }
    else if(m_currentMode == Mode::SetTarget)
    {
        setTarget(cellX, cellY);
    }

    repaint();
}

void Field::paintEvent(QPaintEvent *)
{
    if(graph.numActiveNodes() == 0) return;

    QPainter painter(this);

    painter.setPen(qRgb(0, 0, 0));
    painter.drawRect(0, 0, width() - 1, height() - 1);

    IdxType nodeIdx; QColor brushColor;
    for(uint32_t i = 0; i < m_cellsWidthCount; i++)
    {
        for(uint32_t j = 0; j < m_cellsHeightCount; j++)
        {
            painter.setPen(qRgb(128, 128, 128));

            nodeIdx = j*m_cellsWidthCount + i;

            if(m_busyVector[nodeIdx])
            {
                brushColor = qRgb(0, 0, 255);
            }
            else
            {
                if(m_sourceNode >= 0 && nodeIdx == m_sourceNode)
                {
                    brushColor = qRgb(200, 0, 0);
                }
                else if(m_destNode >= 0 && nodeIdx == m_destNode)
                {
                    brushColor = qRgb(0, 200, 0);
                }
                else
                {
                    brushColor = qRgb(255, 255, 255);
                }
            }

            Graph::NodeType &node = graph.GetNode(nodeIdx);
            if(node.index != invalid_node_index)
            {
                painter.fillRect(i * m_cellWidth, j * m_cellHeight, i * m_cellWidth + m_cellWidth, j * m_cellHeight + m_cellHeight, QBrush(brushColor));
                painter.drawRect(i * m_cellWidth, j * m_cellHeight, i * m_cellWidth + m_cellWidth, j * m_cellHeight + m_cellHeight);

                //painter.setBrush();
                painter.fillRect(node.position.x - m_graphPointRadius, node.position.y - m_graphPointRadius,
                                    m_graphPointRadius * 2, m_graphPointRadius * 2, QBrush(qRgb(0, 255, 255)));

                painter.setPen(qRgb(200, 200, 200));
                const Graph::EdgeList &edges = graph.GetEdges(nodeIdx);
                for (Graph::EdgeList::const_iterator curEdge = edges.begin();
                            curEdge != edges.end();
                            ++curEdge)
                {
                   Graph::NodeType otherNode = graph.GetNode(curEdge->to);
                   painter.drawLine(node.position.x, node.position.y, otherNode.position.x, otherNode.position.y);
                }
            }
            else {
                painter.fillRect(i * m_cellWidth, j * m_cellHeight, i * m_cellWidth + m_cellWidth, j * m_cellHeight + m_cellHeight, QBrush(qRgb(0, 0, 0)));
            }
        }
    }

    painter.setPen(QPen(QBrush(qRgb(0, 0, 255)), 2));

    std::vector<const Edge *> tree = aStar.GetSearchTree();
    for(size_t i = 0; i < tree.size(); i++)
    {
        if(!tree[i])
        {
            continue;
        }

        const Graph::NodeType &start = graph.GetNode(tree[i]->from);
        const Graph::NodeType &end = graph.GetNode(tree[i]->to);

        painter.drawLine(start.position.x, start.position.y, end.position.x, end.position.y);
    }

    painter.setPen(QPen(QBrush(qRgb(255, 0, 0)), 3));

    if(aStar.isPathExist())
    {
        std::list<IdxType> path = aStar.GetPath();

        std::list<IdxType>::iterator it = path.begin();
        std::list<IdxType>::iterator nxt = it; ++nxt;

        for (; nxt != path.end(); ++it, ++nxt)
        {
            if(!graph.isNodePresent(*it) || !graph.isNodePresent(*nxt))
            {
                break;
            }

            const Node& node = graph.GetNode(*it);
            const Node& next = graph.GetNode(*nxt);

            painter.drawLine(node.position.x, node.position.y, next.position.x, next.position.y);
        }
    }
}

void Field::resizeEvent(QResizeEvent *)
{
    m_cellWidth = static_cast<uint32_t>(width()) / m_cellsWidthCount;
    m_cellHeight = static_cast<uint32_t>(height()) / m_cellsHeightCount;
}

void Field::deleteNode(uint32_t x, uint32_t y)
{
    IdxType index = y * m_cellsWidthCount + x;

    if(!graph.isNodePresent(index)) return;

    if(index == m_sourceNode)
    {
        m_sourceNode = -1;
    }

    if(index == m_destNode)
    {
        m_destNode = -1;
    }

    graph.removeNode(index);
}

void Field::addNode(uint32_t x, uint32_t y)
{
    IdxType index = y * m_cellsWidthCount + x;

    if(graph.isNodePresent(index)) return;

    graph.addNode(Node(index, Coordinate2D(x * m_cellWidth + m_cellWidth / 2, y * m_cellHeight + m_cellHeight / 2)));

    Graph_AddAllNeighboursToGridNode(graph, x, y, m_cellsWidthCount, m_cellsHeightCount);
}

void Field::setSource(uint32_t x, uint32_t y)
{
    IdxType index = y * m_cellsWidthCount + x;

    if(!graph.isNodePresent(index)) return;

    m_sourceNode = index;
}

void Field::setTarget(uint32_t x, uint32_t y)
{
    IdxType index = y * m_cellsWidthCount + x;

    if(!graph.isNodePresent(index)) return;

    m_destNode = index;
}

void Field::setMode(Field::Mode mode)
{
    m_currentMode = mode;
}

bool Field::save(const char *fileName)
{
    return graph.save(fileName);
}

bool Field::load(const char *fileName)
{
    return graph.load(fileName);
}
