#ifndef UTILS_H
#define UTILS_H

#include <cmath>
#include <limits>

bool isEqual(double a, double b);

#endif // UTILS_H
