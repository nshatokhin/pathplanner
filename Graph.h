#pragma once

#include "Common.h"
#include "Coordinate2D.h"
#include "utils.h"

#include <fstream>
#include <list>
#include <vector>

#include "Edge.h"
#include "Node.h"

class Graph
{
public:
	//enable easy client access to the edge and node types used in the graph
	typedef Edge                EdgeType;
	typedef Node                NodeType;

	typedef std::vector<NodeType>   NodeVector;
	typedef std::list<EdgeType>     EdgeList;
    typedef std::vector<EdgeList>   EdgeListVector;

public:
	Graph(IdxType maxNodes, bool isDigraph = false);
	~Graph();

	//returns the node at the given index
	const NodeType&  GetNode(IdxType idx)const;

	//non const version
	NodeType&  GetNode(IdxType idx);

	//const method for obtaining a reference to an edge
    const EdgeType& GetEdge(IdxType from, IdxType to) const;

	//non const version
	EdgeType& GetEdge(IdxType from, IdxType to);

    //const method for obtaining a reference to a predecessor edge
    const EdgeType& GetPredecessorEdge(IdxType from, IdxType to) const;

    //non const version
    EdgeType& GetPredecessorEdge(IdxType from, IdxType to);

    // returns all edges from node
    const EdgeList& GetEdges(IdxType node) const;

    //non const version
    EdgeList& GetEdges(IdxType node);

    // returns all edges to node
    const EdgeList& GetPredecessorEdges(IdxType node) const;

    //non const version
    EdgeList& GetPredecessorEdges(IdxType node);

	IdxType getNextFreeNodeIndex() const { return m_nodesCount; }

	IdxType addNode(const Node &node);
	void addEdge(const Edge &edge);

	void removeNode(IdxType node);
	void removeEdge(IdxType from, IdxType to);

	void setEdgeCost(IdxType from, IdxType to, double cost);

	//returns the number of active + inactive nodes present in the graph
	IdxType numNodes() const { return m_nodesCount; }

	//returns the number of active nodes present in the graph (this method's
	//performance can be improved greatly by caching the value)
	IdxType numActiveNodes() const
	{
		int count = 0;

		for (IdxType n = 0; n < m_nodes.size(); ++n) if (m_nodes[n].index != invalid_node_index) ++count;

		return count;
	}

	//returns the total number of edges present in the graph
	IdxType numEdges() const
	{
		int tot = 0;

		for (EdgeListVector::const_iterator curEdge = m_edges.begin();
			curEdge != m_edges.end();
			++curEdge)
		{
			tot += curEdge->size();
		}

		return tot;
	}

	//returns true if the graph is directed
	bool isDigraph() const { return m_digraph; }

	//returns true if the graph contains no nodes
	bool isEmpty() const { return m_nodes.empty(); }

	//returns true if a node with the given index is present in the graph
	bool isNodePresent(IdxType nd) const;

	//returns true if an edge connecting the nodes 'to' and 'from'
	//is present in the graph
	bool isEdgePresent(IdxType from, IdxType to) const;
    bool isPredecessorEdgePresent(IdxType from, IdxType to) const;

	//clears the graph ready for new node insertions
    void Clear()
    {
        m_nodesCount = 0;
        std::fill(m_nodes.begin(), m_nodes.end(), NodeType());
        std::fill(m_edges.begin(), m_edges.end(), EdgeList());
        std::fill(m_predecessorEdges.begin(), m_predecessorEdges.end(), EdgeList());
    }

	void RemoveEdges()
	{
		for (EdgeListVector::iterator it = m_edges.begin(); it != m_edges.end(); ++it)
		{
			it->clear();
		}

        for (EdgeListVector::iterator it = m_predecessorEdges.begin(); it != m_predecessorEdges.end(); ++it)
        {
            it->clear();
        }
	}

    bool save(const char* fileName) const;
    bool save(std::ofstream& stream) const;

    bool load(const char* fileName);
    bool load(std::ifstream& stream);


	//non const class used to iterate through all the edges connected to a specific node. 
	class EdgeIterator
	{
	private:

		typename EdgeList::iterator curEdge;

		Graph&  G;

		const IdxType NodeIndex;

	public:

		EdgeIterator(Graph& graph, IdxType node) : G(graph),
			NodeIndex(node)
		{
			/* we don't need to check for an invalid node index since if the node is
			invalid there will be no associated edges
			*/

			curEdge = G.m_edges[NodeIndex].begin();
		}

		EdgeType*  begin()
		{
			curEdge = G.m_edges[NodeIndex].begin();

			return &(*curEdge);
		}

		EdgeType*  next()
		{
			++curEdge;

            if (!end())
            {
                return &(*curEdge);
            }
            else
            {
                return nullptr;
            }
		}

		//return true if we are at the end of the edge list
		bool end()
		{
            return (curEdge == G.m_edges[NodeIndex].end());
        }
	};

	friend class EdgeIterator;

	//const class used to iterate through all the edges connected to a specific node. 
	class ConstEdgeIterator
	{
	private:

		typename EdgeList::const_iterator curEdge;

		const Graph& G;

		const IdxType NodeIndex;

	public:

		ConstEdgeIterator(const Graph& graph, IdxType node) : G(graph),
			NodeIndex(node)
		{
			/* we don't need to check for an invalid node index since if the node is
			invalid there will be no associated edges
			*/

			curEdge = G.m_edges[NodeIndex].begin();
		}

		const EdgeType*  begin()
		{
			curEdge = G.m_edges[NodeIndex].begin();

			return &(*curEdge);
		}

		const EdgeType*  next()
		{
			++curEdge;

            if (!end())
            {
                return &(*curEdge);
            }
            else
            {
                return nullptr;
            }
		}

		//return true if we are at the end of the edge list
        bool end()
		{
            return (curEdge == G.m_edges[NodeIndex].end());
        }
	};

	friend class ConstEdgeIterator;

    //non const class used to iterate through all the predecessors edges connected to a specific node.
    class PredecessorEdgeIterator
    {
    private:

        typename EdgeList::iterator curEdge;

        Graph&  G;

        const IdxType NodeIndex;

    public:

        PredecessorEdgeIterator(Graph& graph, IdxType node) : G(graph),
            NodeIndex(node)
        {
            /* we don't need to check for an invalid node index since if the node is
            invalid there will be no associated edges
            */

            curEdge = G.m_predecessorEdges[NodeIndex].begin();
        }

        EdgeType*  begin()
        {
            curEdge = G.m_predecessorEdges[NodeIndex].begin();

            return &(*curEdge);
        }

        EdgeType*  next()
        {
            ++curEdge;

            if (!end())
            {
                return &(*curEdge);
            }
            else
            {
                return nullptr;
            }
        }

        //return true if we are at the end of the edge list
        bool end()
        {
            return (curEdge == G.m_predecessorEdges[NodeIndex].end());
        }
    };

    friend class PredecessorEdgeIterator;

    //const class used to iterate through all the predecessors edges connected to a specific node.
    class ConstPredecessorEdgeIterator
    {
    private:

        typename EdgeList::const_iterator curEdge;

        const Graph& G;

        const IdxType NodeIndex;

    public:

        ConstPredecessorEdgeIterator(const Graph& graph, IdxType node) : G(graph),
            NodeIndex(node)
        {
            /* we don't need to check for an invalid node index since if the node is
            invalid there will be no associated edges
            */

            curEdge = G.m_predecessorEdges[NodeIndex].begin();
        }

        const EdgeType*  begin()
        {
            curEdge = G.m_predecessorEdges[NodeIndex].begin();

            return &(*curEdge);
        }

        const EdgeType*  next()
        {
            ++curEdge;

            if (!end())
            {
                return &(*curEdge);
            }
            else
            {
                return nullptr;
            }
        }

        //return true if we are at the end of the edge list
        bool end()
        {
            return (curEdge == G.m_predecessorEdges[NodeIndex].end());
        }
    };

    friend class ConstPredecessorEdgeIterator;

	//non const class used to iterate through the nodes in the graph
	class NodeIterator
	{
	private:

		typename NodeVector::iterator         curNode;

		Graph& G;

		//if a graph node is removed, it is not removed from the 
		//vector of nodes (because that would mean changing all the indices of 
		//all the nodes that have a higher index). This method takes a node
		//iterator as a parameter and assigns the next valid element to it.
		void getNextValidNode(typename NodeVector::iterator& it)
		{
			if (curNode == G.m_nodes.end() || it->index != invalid_node_index) return;

			while ((it->index == invalid_node_index))
			{
				++it;

				if (curNode == G.m_nodes.end()) break;
			}
		}

	public:

		NodeIterator(Graph &graph) : G(graph)
		{
			curNode = G.m_nodes.begin();
		}


		NodeType* begin()
		{
			curNode = G.m_nodes.begin();

			getNextValidNode(curNode);

			return &(*curNode);
		}

		NodeType* next()
		{
			++curNode;

			getNextValidNode(curNode);

			return &(*curNode);
		}

		bool end()
		{
			return (curNode == G.m_nodes.end());
		}
	};


	friend class NodeIterator;

	//const class used to iterate through the nodes in the graph
	class ConstNodeIterator
	{
	private:

		typename NodeVector::const_iterator			curNode;

		const Graph& G;

		//if a graph node is removed or switched off, it is not removed from the 
		//vector of nodes (because that would mean changing all the indices of 
		//all the nodes that have a higher index. This method takes a node
		//iterator as a parameter and assigns the next valid element to it.
		void getNextValidNode(typename NodeVector::const_iterator& it)
		{
			if (curNode == G.m_nodes.end() || it->index != invalid_node_index) return;

			while ((it->index == invalid_node_index))
			{
				++it;

				if (curNode == G.m_nodes.end()) break;
			}
		}

	public:

		ConstNodeIterator(const Graph &graph) : G(graph)
		{
			curNode = G.m_nodes.begin();
		}


		const NodeType* begin()
		{
			curNode = G.m_nodes.begin();

			getNextValidNode(curNode);

			return &(*curNode);
		}

		const NodeType* next()
		{
			++curNode;

			getNextValidNode(curNode);

			return &(*curNode);
		}

		bool end()
		{
			return (curNode == G.m_nodes.end());
		}
	};

	friend class ConstNodeIterator;

protected:
	bool isUniqueEdge(IdxType from, IdxType to);
    bool isUniquePredecessorEdge(IdxType from, IdxType to);
	void removeInvalidEdges();

protected:
	bool m_digraph;

	IdxType m_maxNodesCount;
	IdxType m_nodesCount;

	NodeVector m_nodes;
	EdgeListVector m_edges;
    EdgeListVector m_predecessorEdges;
};

