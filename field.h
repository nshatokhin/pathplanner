#ifndef FIELD_H
#define FIELD_H

#include <QWidget>

#include "Graph.h"
#include "dfssearch.h"
#include "bfssearch.h"
#include "dijkstrasearch.h"
#include "astarsearch.h"
#include "DStarLiteSearch.h"
#include "heuristic.h"

class Field : public QWidget
{
    Q_OBJECT

public:
    enum class Mode : uint8_t
    {
        DeleteNode = 0,
        AddNode,
        SetSource,
        SetTarget
    };

public:
    explicit Field(QWidget *parent = nullptr);

    void init(uint32_t cellsXCount, uint32_t cellsYCount);
    void search();

    void setMode(Mode mode);

    bool save(const char * fileName);
    bool load(const char * fileName);

protected:
    void mousePressEvent(QMouseEvent *event);
    void paintEvent(QPaintEvent *event);
    void resizeEvent(QResizeEvent *event);

    void deleteNode(uint32_t x, uint32_t y);
    void addNode(uint32_t x, uint32_t y);
    void setSource(uint32_t x, uint32_t y);
    void setTarget(uint32_t x, uint32_t y);

protected:
    static constexpr uint32_t m_graphPointRadius = 3;

    uint32_t m_cellsWidthCount;
    uint32_t m_cellsHeightCount;
    uint32_t m_cellWidth;
    uint32_t m_cellHeight;

    Mode m_currentMode;

    int64_t m_sourceNode, m_destNode;

    Graph graph;
    DFSSearch dfs;
    BFSSearch bfs;
    DijkstraSearch deikstra;
    AStarSearch<HeuristicEuclid> aStar;
    DStarLiteSearch<HeuristicEuclid> dStar;

    std::vector<bool> m_busyVector;

signals:

public slots:
};

#endif // FIELD_H
